angular.module('app')
    .controller('incluiController', ['$location', '$http', '$timeout', function($location, $http, $timeout){
        var self = this;
        self.boxsucesso = false;

        self.incluiProduto = function(novoProduto){
            if (novoProduto) novoProduto.status = false;
            $http.post('http://localhost:3000/compras', novoProduto)
                .then(
                    function(response){
                        console.log(response.data);
                        self.boxSucesso = true;
                        self.novoProduto.nome = '';
                        self.novoProduto.qtdeNecessaria = '';
                        self.novoProduto.qdteDispensa = '';
                        self.novoProduto.valor = '';
                        self.novoProduto.responsavel = '';
                        $timeout(function(){
                            self.boxSucesso = false;}, 2000);
                    },
                    function(erro){
                        console.log(erro);
                    }
                );
        };



    }]);