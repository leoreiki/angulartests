/**
 * Created by Icons4u on 2/5/16.
 */

angular.module('app')
    .controller('listaController', ['$location', '$http', '$filter', function($location, $http, $filter){
        var self = this;

        var urlDoJsonCompras = 'http://localhost:3000/compras/';
        var urlDoJsonValores = 'http://localhost:3000/valores/';


        self.Total = 0;
        self.titulo = 'Lista de Compras';

        self.variavelCarrinho = 0;
        // Lista todos os produtos da lista
        self.listaItens = function(){
            $http.get(urlDoJsonCompras, {})
                .then(
                    function(response){
                        self.items = response.data;

                    },
                    function(erro){
                        console.log(erro);
                    }
                );
        };
        self.listaItens();


        // Deleta um produto da lista
        self.deletaProduto = function(produto){
            var index = self.items.indexOf(produto);
            if (index != -1){
                $http.delete(urlDoJsonCompras+produto.id, {})
                    .then(
                        function(response){
                            self.items.splice(index,1);
                            console.log(response.data);
                            self.listaItens();
                        },
                        function(erro){
                            console.log(erro);
                        }
                    );
            };
        };

        // Abre o menu de opções de cada produto
        self.boxAcoes = [];
        self.abreBox = function(index, produto){
            self.boxAcoes = [];
            self.boxAcoes[index] = true;
        };

        // Alterna entre nome e input quando o botão "Editar" é clicado
        self.camposAbertos = [];
        self.editarProduct = function(index, produto){

            self.camposAbertos = [];
            self.camposAbertos[index] = true;
        };


        // Atualiza as informações de um produto
        self.atualizaProduct = function(index, produto){
            $http.patch(urlDoJsonCompras+produto.id, {nome:produto.nome, qtdeNecessaria:produto.qtdeNecessaria, qdteDispensa:produto.qdteDispensa, valor:produto.valor, responsavel:produto.responsavel, status:produto.status})
                .then(
                    function(response){
                        self.camposAbertos[index] = false;
                        self.boxAcoes[index] = false;
                        console.log(response.data);
                        if (produto.status == true){
                            self.TotalCarrinho = self.TotalCarrinho + produto.valor * (produto.qtdeNecessaria - produto.qdteDispensa);
                            self.atualizaValorCarrinho(self.TotalCarrinho);
                        };
                        if (produto.status == false){
                            self.TotalCarrinho = self.TotalCarrinho - produto.valor * (produto.qtdeNecessaria - produto.qdteDispensa);
                            self.atualizaValorCarrinho(self.TotalCarrinho);
                        };
                        //self.atualizaValorCarrinho(self.TotalCarrinho);
                    },
                    function(erro){
                        console.log(erro);
                    }
                );
        };

        // Função do botão "Cancelar" que fecha box de ações e volta estado de edição quando for o caso
        self.cancelAction = function(index, produto){
            self.camposAbertos[index] = false;
            self.boxAcoes[index] = false;
        };


        self.carregarUmValor = function() {
            self.TotalCarrinho = 0;
            $http.get(urlDoJsonValores+'1', {})
                .then(
                    function(response){
                        self.TotalCarrinho = response.data.valor;
                        console.log(self.TotalCarrinho);
                    },
                    function(erro){
                        console.log(erro);
                    }
                );

        };
        self.carregarUmValor();


        // Atualiza Valor no Carrinho
        self.atualizaValorCarrinho = function(proo){
            $http.patch(urlDoJsonValores+'1', {valor:proo})
                .then(
                    function(response){
                        console.log(response.data);
                    },
                    function(erro){
                        console.log(erro);
                    }
                );
        };

    }]);