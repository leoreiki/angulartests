angular.module('app', [
    'ngRoute'
])
    .config(function($routeProvider){
        $routeProvider
            .when('/', {
                templateUrl:'views/main.html',
                controller:'mainController',
                controllerAs: 'main'
            })
            .when('/lista-de-compras',{
                templateUrl:'views/lista-de-compras.html',
                controller: 'listaController',
                controllerAs: 'lista'
            })
            .when('/inclui-item',{
                templateUrl:'views/inclui-item.html',
                controller: 'incluiController',
                controllerAs: 'inclui'
            })
    });