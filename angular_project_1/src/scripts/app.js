'use strict';
console.log(angular);

angular.module('app',[
    'ngRoute'
])
    .config(function($routeProvider){
        $routeProvider
            .when('/',{
                templateUrl: 'src/views/main.html',
                controller: 'MainController',
                controllerAs: 'main'
            })
            .when('/contact',{
                templateUrl: 'src/views/contact.html',
                controller: 'ContactController',
                controllerAs: 'contact'
            });
    });
