angular.module('app')
    // no caso acima, adeclaração não cria um novo controller pois não existe um array de módulos sendo chamado, assim, estamos apenas chamando o controller app nesse controller.
    .controller('MainController',['$location', function($location){
        var self = this;
        self.title = 'dsfsdfddf';
        self.keyWords = ['A','B','C'];
        self.name = "Leonardo";
        self.showAlert = function(){
            alert('Fui Clicado!');
            console.log('alguém me clicou ai...');
        }
        self.nextPage = function(){
            $location.path('/contact');
        }
        self.emps = ['Leonardo', 'Denis', 'Alysson', 'Wellington'];
        self.emps2 = [
            {name:'Leonardo', nickname:'LL'},
            {name:'Denis', nickname:'DD'},
            {name:'Alysson', nickname:'AA'},
            {name:'Wellington', nickname:'WW'}
        ];
        self.emp3 = {name: 'Leonardo', nickname: 'L'};

        self.value = 25.25;

        self.currentUser = {
            name: 'Leonardo',
            email: 'leoreiki@gmail.com',
            status: ''
        };

    }]);