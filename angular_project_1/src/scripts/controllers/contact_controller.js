angular.module('app')
    .controller('ContactController', function(){
        var self = this;
        self.title = 'Página de Contato';
        self.subtitle = 'Utilize essa página para entrar em contato conosco';
        self.nameform = '';
        self.email = '';
        self.textarea = '';
        self.preview = false;
        self.showPreview = function(){
            self.preview = true;
        }
    });