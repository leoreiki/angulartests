/**
 * Created by Icons4u on 1/27/16.
 */

angular.module ('app', [
    'ngRoute'
])
    .config(function($routeProvider){
        $routeProvider
            .when('/',{
                templateUrl: 'src/views/main.html',
                controller: 'mainController',
                controllerAs: 'main'
            })
            .when('/produto',{
                templateUrl: 'src/views/produto.html',
                controller: 'productController',
                controllerAs: 'product'
            })
            .when('/contato', {
                templateUrl: 'src/views/contato.html',
                controller: 'contactController',
                controllerAs: 'contact'
            })
    });