angular.module('app')
    .controller('productController',['$location', '$scope' , '$filter' , '$http', function($location, $scope, $filter ,  $http){
        var self = this;
        self.title = 'Página de Produto';



        self.deleteWish = function (produto) {
            var index = self.items.indexOf(produto);
            if (index != -1) {
                $http.delete('http://10.10.10.193:7001/items/'+produto._id, {})
                    .then(
                        function(response){
                            alert(response.data);
                            self.items.splice(index,1);
                        }
                        ,
                        function(erro){
                            alert(erro);
                            console.log(erro);
                        }
                    );
            }
        };

        self.addItem = function (novoProduto) {
            $http.post('http://10.10.10.193:7001/items', novoProduto)
                .then(
                    function(response){
                        alert(response.data);
                        self.getAll();
                    }
                    ,
                    function(erro){
                        console.log(erro);
                    }
                );
        };

        self.updateProduct = function (produto){
            $http.patch('http://10.10.10.193:7001/items/'+produto._id, {name:produto.name, peso:produto.peso})
                .then(
                    function(response){
                        alert(response.data);
                        self.getAll();
                        self.edicao = [];
                    }
                    ,
                    function(erro){
                        console.log(erro);
                    }
                );
        };

        self.getAll = function (){
            $http.get('http://10.10.10.193:7001/items', {})
                .then(
                    function(response){
                        self.items = response.data;
                    }
                    ,
                    function(erro){
                        console.log(erro);
                    }
                );
        };

        self.getAll();

        self.preview = false;
        self.showPreview = function(){
            self.preview = true;
        }

        var orderBy = $filter('orderBy');
        self.order = function(predicate) {
            self.predicate = predicate;
            self.reverse = !self.reverse;
        };

        self.edicao = [];
        self.showEdicao = function(index, produto){
            self.edicao = [];
            self.edicao[index] = true;
            self.produtoTemp = {};
            self.produtoTemp.name = produto.name;
            self.produtoTemp.peso = produto.peso;
            self.produtoTemp._id = produto._id;
        }


    }]);

