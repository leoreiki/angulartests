angular.module('app')
    .controller('listagemController',['$location', '$scope', '$http', '$timeout', function($location, $scope, $http, $timeout){
        var self = this;
        self.title = 'Listagem geral';
        self.description = 'Listagem geral';
        self.boxMensagem = false;

        // Objeto que pode ser puxado diretamente
        self.artigos = [
            {tipo:'Camisa', cor:'Azul', tamanho:'M', id:'0001'},
            {tipo:'Camisa', cor:'Branca', tamanho:'G', id:'0002'},
            {tipo:'Calça', cor:'Lilás', tamanho:'P', id:'0003'},
            {tipo:'Casaco', cor:'Verde', tamanho:'M', id:'0004'},
            {tipo:'Blusa', cor:'Vermelha', tamanho:'G', id:'0005'},
            {tipo:'Short', cor:'Verde', tamanho:'P', id:'0006'},
            {tipo:'Bermuda', cor:'Branca', tamanho:'P', id:'0007'},
            {tipo:'Casaco', cor:'Laranja', tamanho:'M', id:'0008'},
            {tipo:'Calça', cor:'Vermelha', tamanho:'G', id:'0009'},
            {tipo:'Blusa', cor:'Rosa', tamanho:'P', id:'0010'}
        ];


        // Exemplo de Json sendo puxado para o controller, exibindo as informações na view
        self.getAll = function (){
            $http.get('http://localhost:3000/posts', {})
                .then(
                    function(response){
                        self.items = response.data;
                    }
                    ,
                    function(erro){
                        console.log(erro);
                    }
                );
        };
        self.getAll();

        self.deleteProduto = function(produto){
            var index = self.items.indexOf(produto);
            if (index != -1){
                $http.delete('http://localhost:3000/posts/'+produto.id, {})
                    .then(
                        function(response){
                            self.items.splice(index,1);
                        },
                        function(erro){
                            console.log(erro);
                        }
                    );
            }

        };



        self.updateProduct = function (produto){
            $http.patch('http://localhost:3000/posts/'+produto.id, {tipo:produto.tipo, cor:produto.cor, tamanho:produto.tamanho})
                .then(
                    function(response){
                        alert(response.data);
                        self.getAll();
                        self.boxMensagem = true;
                        $timeout(function(){
                            self.boxMensagem = false;
                        }, 2000);
                        self.edicao = [];
                    }
                    ,
                    function(erro){
                        console.log(erro);
                    }
                );
        };


        self.edicao = [];
        self.showEdicao = function(index, produto){
            self.edicao = [];
            self.edicao[index] = true;
            self.produtoTemp = {};
            self.produtoTemp.id = produto.id;
            self.produtoTemp.tipo = produto.tipo;
            self.produtoTemp.cor = produto.cor;
            self.produtoTemp.tamanho = produto.tamanho;
        }



    }]);
