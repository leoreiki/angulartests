angular.module('app')
    .controller('atualizaItemController', ['$location', '$http', $stateParams, function($location, $http, $stateParams){
        var self = this;
        self.titulo = "Atualização de Item";
        self.boxMensagem = false;


        self.carregarUmProduto = function(idDoProduto){
            $http.get('http://localhost:3000/posts/'+idDoProduto)
                .then(
                    function(response) {
                        console.log(reponse.data);
                        self.produtoInfo = response.data;
                    },
                    function(erro) {
                        console.log(erro);
                    }
                );
        };
        self.carregarUmProduto(idDoProduto);


        self.atualizacaoItem = function(prod){
            $http.patch('http://localhost:3000/posts/'+prod.id, {tipo:prod.tipo, cor:prod.cor, tamanho:prod.tamanho})
                .then(
                    function(response){
                        console.log(response.data);
                        self.boxMensagem = true;
                    },
                    function(erro){
                        console.log(erro);
                    }
                );

        };

}]);