angular.module('app')
    .controller('incluiItemController',['$location', '$http', function($location, $http){
        var self = this;
        self.title = 'Inclusão de Item';
        self.description = 'Página de Inclusão de Item';

        self.adicionarProduto = function(novoProduto){
            $http.post('http://localhost:3000/posts', novoProduto)
                .then(
                    function(response){
                        console.log(response.data);
                        self.voltarListagem();
                    },
                    function(erro){
                        console.log(erro);
                    }

                );
        };

        self.voltarListagem = function(){
            $location.path('/listagem');
        };

    }]);