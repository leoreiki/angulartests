/**
 * Created by Icons4u on 2/2/16.
 */

angular.module ('app', [
    'ngRoute'
])
    .config(function($routeProvider){
        $routeProvider
            .when('/',{
                templateUrl: 'src/views/main.html',
                controller: 'mainController',
                controllerAs: 'main'
            })
            .when('/listagem',{
                templateUrl: 'src/views/listagem.html',
                controller: 'listagemController',
                controllerAs: 'listagem'
            })
            .when('/incluir-item', {
                templateUrl: 'src/views/incluir-item.html',
                controller: 'incluiItemController',
                controllerAs: 'incluiItem'
            })


    });