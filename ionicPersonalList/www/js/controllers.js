angular.module('starter.controllers', [])

.controller('listaController', function($scope, $location, $http, $filter) {
  var self = this;

  var urlDoJsonCompras = 'http://localhost:3000/compras/';
  var urlDoJsonValores = 'http://localhost:3000/valores/';


  self.Total = 0;
  self.titulo = 'Lista de Compras';

  self.variavelCarrinho = 0;
  // Lista todos os produtos da lista
  self.listaItens = function(){
    $http.get(urlDoJsonCompras, {})
      .then(
        function(response){
          self.items = response.data;

        },
        function(erro){
          console.log(erro);
        }
      );
  };
  self.listaItens();


  // Deleta um produto da lista
  self.deletaProduto = function(produto){
    var index = self.items.indexOf(produto);
    if (index != -1){
      $http.delete(urlDoJsonCompras+produto.id, {})
        .then(
          function(response){
            self.items.splice(index,1);
            console.log(response.data);
            self.listaItens();
          },
          function(erro){
            console.log(erro);
          }
        );
    };
  };

  // Abre o menu de opções de cada produto
  self.boxAcoes = [];
  self.abreBox = function(index, produto){
    self.boxAcoes = [];
    self.boxAcoes[index] = true;
  };

  // Alterna entre nome e input quando o botão "Editar" é clicado
  self.camposAbertos = [];
  self.editarProduct = function(index, produto){

    self.camposAbertos = [];
    self.camposAbertos[index] = true;
  };


  // Atualiza as informações de um produto
  self.atualizaProduct = function(index, produto){
    $http.patch(urlDoJsonCompras+produto.id, {nome:produto.nome, qtdeNecessaria:produto.qtdeNecessaria, qdteDispensa:produto.qdteDispensa, valor:produto.valor, responsavel:produto.responsavel, status:produto.status})
      .then(
        function(response){
          self.camposAbertos[index] = false;
          self.boxAcoes[index] = false;
          console.log(response.data);
        },
        function(erro){
          console.log(erro);
        }
      );
  };

  // Marca um Produto (Altera status)
  self.marcaProduto = function(index, produto){
    $http.patch(urlDoJsonCompras+produto.id, {status:produto.status})
      .then(
        function(response){
          console.log(response.data);
          if (produto.status == true){
            self.TotalCarrinho = self.TotalCarrinho + produto.valor * (produto.qtdeNecessaria - produto.qdteDispensa);
            self.atualizaValorCarrinho(self.TotalCarrinho);
          };
          if (produto.status == false){
            self.TotalCarrinho = self.TotalCarrinho - produto.valor * (produto.qtdeNecessaria - produto.qdteDispensa);
            self.atualizaValorCarrinho(self.TotalCarrinho);
          };
          //self.atualizaValorCarrinho(self.TotalCarrinho);
        },
        function(erro){
          console.log(erro);
        }
      );
  };


  // Desmarcar todos os itens
  self.desmarcaTodos = function(index, produto){
    $http.patch(urlDoJsonCompras+produto.id, {status:produto.status})
      .then(
        function(response){
          console.log(response.data);


          self.TotalCarrinho = 0
          self.atualizaValorCarrinho(self.TotalCarrinho);
        },
        function(erro){
          console.log(erro);
        }
      );
  };





  // Função do botão "Cancelar" que fecha box de ações e volta estado de edição quando for o caso
  self.cancelAction = function(index, produto){
    self.camposAbertos[index] = false;
    self.boxAcoes[index] = false;
  };


  self.carregarUmValor = function() {
    self.TotalCarrinho = 0;
    $http.get(urlDoJsonValores+'1', {})
      .then(
        function(response){
          self.TotalCarrinho = response.data.valor;
          console.log(self.TotalCarrinho);
        },
        function(erro){
          console.log(erro);
        }
      );

  };
  self.carregarUmValor();


  // Atualiza Valor no Carrinho
  self.atualizaValorCarrinho = function(proo){
    $http.patch(urlDoJsonValores+'1', {valor:proo})
      .then(
        function(response){
          console.log(response.data);
        },
        function(erro){
          console.log(erro);
        }
      );
  };
})





.controller('incluiController', function($scope, $location, $http, $timeout) {
  var self = this;
  self.boxsucesso = false;

  self.incluiProduto = function(novoProduto){
    if (novoProduto) novoProduto.status = false;
    $http.post('http://localhost:3000/compras', novoProduto)
      .then(
        function(response){
          console.log(response.data);
          self.boxSucesso = true;
          self.novoProduto.nome = '';
          self.novoProduto.qtdeNecessaria = '';
          self.novoProduto.qdteDispensa = '';
          self.novoProduto.valor = '';
          self.novoProduto.responsavel = '';
          $timeout(function(){
            self.boxSucesso = false;}, 4000);
        },
        function(erro){
          console.log(erro);
        }
      );
  };
})


.controller('AccountCtrl', function($scope, $location, $http, $ionicPopup) {
  var self = this;
  var urlDoJsonCompras = 'http://localhost:3000/compras/';
  var urlDoJsonValores = 'http://localhost:3000/valores/';


  $scope.settings = {
    enableFriends: true
  };

  // Lista todos os produtos da lista
  self.listaItens = function(){
    $http.get(urlDoJsonCompras, {})
      .then(
        function(response){
          self.items = response.data;

        },
        function(erro){
          console.log(erro);
        }
      );
  };
  self.listaItens();



  // Deleta um produto da lista
  self.deletaProduto = function(produto){
    var number = 0;
    do {
      for (itt in self.items) {
        $http.delete(urlDoJsonCompras + number++, {})
          .then(
            function (response) {
              self.items.splice();
              console.log(response.data);
            },
            function (erro) {
              console.log(erro);
            }
          );
        console.log(itt.id);
      };
      console.log("clique");
    }
    while (number < self.items.lenght);
  };



  // Janela de confirmação de função
  self.showConfirm = function(produto) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Remover todos os produtos',
      template: 'Você deseja realmente remover todos os produtos?'
    });

    confirmPopup.then(function(res) {
      if(res) {
        self.deletaProduto(produto)
        console.log('Clicou em ok');
      } else {
        console.log('Clicou em cancelar');
      }
    });
  };





})



