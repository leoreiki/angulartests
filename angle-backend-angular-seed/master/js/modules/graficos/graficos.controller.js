(function () {
    'use strict';

    angular
        .module('app.graficos')
        .controller('produtosGraficosController', produtosGraficosController);

    produtosGraficosController.$inject = ['$scope'];
    function produtosGraficosController($scope) {

        var self = this;

        self.areaData =
            [{
                "label": "Uniques",
                "color": "#aad874",
                "data": [
                    ["Mar", 50],
                    ["Apr", 84],
                    ["May", 52],
                    ["Jun", 88],
                    ["Jul", 69],
                    ["Aug", 92],
                    ["Sep", 58]
                ]
            }, {
                "label": "Recurrent",
                "color": "#7dc7df",
                "data": [
                    ["Mar", 13],
                    ["Apr", 44],
                    ["May", 44],
                    ["Jun", 27],
                    ["Jul", 38],
                    ["Aug", 11],
                    ["Sep", 39]
                ]
            },  {
                "label": "Itens",
                "color": "#f00",
                "data": [
                    ["Mar", 34],
                    ["Apr", 45],
                    ["May", 22],
                    ["Jun", 57],
                    ["Jul", 48],
                    ["Aug", 31],
                    ["Sep", 89]
                ]

            }];
        self.areaOptions = {
            series: {
                lines: {
                    show: true,
                    fill: 0.8
                },
                points: {
                    show: true,
                    radius: 4
                }
            },
            grid: {
                borderColor: '#eee',
                borderWidth: 1,
                hoverable: true,
                backgroundColor: '#fcfcfc'
            },
            tooltip: true,
            tooltipOpts: {
                content: function (label, x, y) {
                    return x + ' : ' + y;
                }
            },
            xaxis: {
                tickColor: '#fcfcfc',
                mode: 'categories'
            },
            yaxis: {
                min: 0,
                tickColor: '#eee',
                position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                tickFormatter: function (v) {
                    return v + ' visitors';
                }
            },
            shadowSize: 0
        };

    }
})();

