(function() {
    'use strict';

    angular
        .module('app.produtos')
        .service('produtosDaoService', produtosDaoService);

    produtosDaoService.$inject = ['$http'];
    function produtosDaoService($http) {
        // for controllerAs syntax
        var self = this;

        self.teste = function(){
            return 'O grande teste';
        }
        // A função criada nesse serviço retorna a mensagem.

        var urlDoServidor = 'http://10.10.10.193:7001';

        self.carregarTodosProdutos = function(){
            return $http.get(urlDoServidor+'/items',{});
        }
        // Essa função foi simplificada em relação ao projeto anterior, e ela apenas "carrega" o json.

        self.carregarUmProduto = function(produtoId){
            return $http.get(urlDoServidor+'/items/'+produtoId,{});
        }

        self.adicionaUmProduto = function(novoProduto){
            return $http.post(urlDoServidor+'/items', novoProduto);
        }

        self.deletaUmProduto = function(produto){
            return $http.delete(urlDoServidor+'/items/'+produto._id, {});
        }

        self.atualizaUmProduto = function(prod){
            return $http.patch(urlDoServidor+'/items/'+prod._id, {name:prod.name, peso:prod.peso})
        }

    }
})();

