(function() {
    'use strict';

    angular
        .module('app.produtos')
        .controller('produtosEditarController', produtosEditarController);

    produtosEditarController.$inject = ['produtosDaoService','$stateParams', '$state'];
    function produtosEditarController(produtosDaoService,$stateParams, $state) {

        var self = this;

        var idDoProduto = $stateParams.produtoId;

        // Função que recupera a informação e depois é chamadada para listar as informações do produto
        self.editingItem = function (idDoProduto) {
            produtosDaoService.carregarUmProduto(idDoProduto)
                .then(
                    function(response){
                        console.log(response.data);
                        self.produtoInfo = response.data;

                        //alert(response.data);
                    }
                    ,
                    function(erro){
                        console.log(erro);
                    }
                );
        };
        self.editingItem(idDoProduto);


        // Função que envia as informações novas para o servidor
        self.atualizaProduto = function (prod) {
            produtosDaoService.atualizaUmProduto(prod)
                .then(
                    function(response){
                        console.log(response.data);
                        $state.go('app.listProducts');
                    }
                    ,
                    function(erro){
                        console.log(erro);
                    }
                );
        };



    }
})();

