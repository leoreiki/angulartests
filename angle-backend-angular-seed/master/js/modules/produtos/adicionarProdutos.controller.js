(function() {
    'use strict';

    angular
        .module('app.produtos')
        .controller('produtosAdicionarController', produtosAdicionarController);

    produtosAdicionarController.$inject = ['produtosDaoService'];
    function produtosAdicionarController(produtosDaoService) {

        var self = this;

        self.addItem = function (novoProduto) {
            produtosDaoService.adicionaUmProduto(novoProduto)
                .then(
                    function(response){
                        alert(response.data);
                        self.novoProduto.name = '';
                        self.novoProduto.peso = '';
                        //self.getAll();
                    }
                    ,
                    function(erro){
                        console.log(erro);
                    }
                );
        };



    }
})();

