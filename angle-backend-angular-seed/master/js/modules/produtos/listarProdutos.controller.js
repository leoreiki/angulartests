(function() {
    'use strict';

    angular
        .module('app.produtos')
        .controller('produtosListarController', produtosListarController);

    produtosListarController.$inject = ['produtosDaoService','$state'];
    function produtosListarController(produtosDaoService,$state) {

        var self = this;

        var respostaDoServico = produtosDaoService.teste();
        // A variável acima recebe o resultado da função que foi criada no arquivo produtos-dao.service.js

        console.log(respostaDoServico);
        // o console acima exibe a mensagem gravada na variável logo acima

        produtosDaoService.carregarTodosProdutos()
            .then(
                function(response){
                    console.log(response.data);
                    self.items = response.data;
                }
                ,
                function(erro){
                    console.log(erro);
                }
            );

        produtosDaoService.carregarUmProduto('YN3dQcexXta0EcqY')
            .then(
                function(response){
                    console.log(response.data);
                }
                ,
                function(erro){
                    console.log(erro);
                }
            );

        self.getAll = function(){
            produtosDaoService.carregarTodosProdutos()
                .then(
                    function(response){
                        self.items = response.data;
                    }
                    ,
                    function(erro){
                        console.log(erro);
                    }
                );

        };
        self.getAll();

        self.deleteProduct = function (produto) {
            var index = self.items.indexOf(produto);
            if (index != -1) {
                produtosDaoService.deletaUmProduto(produto)
                    .then(
                        function(response){
                            alert(response.data);
                            self.items.splice(index,1);
                        }
                        ,
                        function(erro){
                            alert(erro);
                            console.log(erro);
                        }
                    );
            }
        };

        self.editProduct = function (id){
            $state.go('app.viewProducts',{produtoId:id});
        };
    }
})();

