/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 =========================================================*/


(function() {
    'use strict';

    angular
        .module('app.routes')
        .config(routesConfig);

    routesConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider'];
    function routesConfig($stateProvider, $locationProvider, $urlRouterProvider, helper){
        
        // Set the following to true to enable the HTML5 Mode
        // You may have to set <base> tag in index and a routing configuration in your server
        $locationProvider.html5Mode(false);

        // defaults to dashboard
        $urlRouterProvider.otherwise('/app/singleview');

        // 
        // Application Routes
        // -----------------------------------   
        $stateProvider
          .state('app', {
              url: '/app',
              abstract: true,
              templateUrl: helper.basepath('app.html'),
              resolve: helper.resolveFor('modernizr', 'icons')
          })
          .state('app.singleview', {
              url: '/singleview',
              title: 'Single View',
              templateUrl: helper.basepath('singleview.html')
          })
          .state('app.listProducts', {
              url: '/listar-produtos',
              title: 'Listar Produtos',
              controller: 'produtosListarController',
              controllerAs: 'produtos',
              templateUrl: helper.basepath('listar-produtos.html')
          })
          .state('app.addProducts', {
              url: '/inserir-produtos',
              title: 'Inserir Produtps',
              controller: 'produtosAdicionarController',
              controllerAs: 'addproduto',
              templateUrl: helper.basepath('adicionar-produtos.html')
          })
          .state('app.viewProducts', {
              url: '/editar-produto/:produtoId',
              title: 'Editar Produto',
              controller: 'produtosEditarController',
              controllerAs: 'produto',
              templateUrl: helper.basepath('editar-produto.html')
          })
          .state('app.graficos', {
              url: '/graficos',
              title: 'Gráficos',
              controller: 'produtosGraficosController',
              controllerAs: 'graficos',
              templateUrl: helper.basepath('graficos.html')
          })
          .state('app.submenu', {
              url: '/submenu',
              title: 'Submenu',
              templateUrl: helper.basepath('submenu.html')
          })
          // 
          // CUSTOM RESOLVES
          //   Add your own resolves properties
          //   following this object extend
          //   method
          // ----------------------------------- 
          // .state('app.someroute', {
          //   url: '/some_url',
          //   templateUrl: 'path_to_template.html',
          //   controller: 'someController',
          //   resolve: angular.extend(
          //     helper.resolveFor(), {
          //     // YOUR RESOLVES GO HERE
          //     }
          //   )
          // })
          ;

    } // routesConfig

})();

